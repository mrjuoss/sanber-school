<!-- {{ $data }} -->

<h1>Halaman Comment.index</h1>
    

    <div class="ml-3 mt-3">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Comment List</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <table class="table table-bordered table-striped" rules="none" border="1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Commentar</th>
                    <th>Judul Berita</th>
                    <th>Tanggal Update Komentar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $comment)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $comment->nama }}</td>
                    <td>{{ $comment->body }}</td>
                    <td>{{ $comment->news->title }}</td>
                    <td>{{ $comment->updated_at }}</td>
                    
                </tr>
                @endforeach
            </tbody>
            </table>

          </form>
        </div>
    </div>

