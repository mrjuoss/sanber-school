<div class="section-title">
            <h2>NEWS</h2>
            <p>Detail Berita dan Artikel yang ditulis oleh Para Trainer Sanber School</p>
        </div>
        <div class="row">
            <div class="mb-5 col-xl-12 col-md-12 d-flex flex-column align-items-stretch" data-aos="zoom-in"
                data-aos-delay="100">
                <img src="{{ asset('public/news/'.$data->photo) }}" alt="News Image" class="img-fluid" />
                <div class="mt-2">
                    <h2>{{ $data->title }}</h2>
                    <p>{!! $data->body !!}</p>
                </div>

                <div class="mt-4">
                    <p>Ditulis oleh : {{ $data->user['name'] }}</p>
                    <p>Ditulis Tanggal : {{ $data->created_at }}</p>
                    <p>Diubah Tanggal : {{ $data->updated_at }}</p>
                </div>
            </div>
        </div>

        <!-- start Makki -->
            <!-- Social sharing buttons -->
             
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments"> 
                <br>
                <?php
                    $likes=mt_rand(101,999);
                ?>
                <span class="float-right text-muted">{{$likes}} likes - {{count($comment)}} comments</span>
                <br>
                <div class="card-comment">
                
                  <!-- /.comment-text -->  
                  @forelse($comment as $komentar)               
                  <div class="card-footer card-comments">                    
                        <!-- /.card-comment -->
                        <div class="card-comment">
                        
                            <div class="comment-text">
                                <p>
                                    {{ $komentar->body }}
                                    <br>                            
                                    <span class="text-muted float-right"><a href="#"> {{ $komentar->nama }}</a>, {{$komentar->updated_at}}</span> 
                                    <br>                           
                                </p>
                            </div>
                        <!-- /.comment-text -->
                        </div>
                        <!-- /.card-comment -->
                    </div>

                    @empty
                        <p align="center"> - No comment - </p>

                    <br>
                    @endforelse

                </div>
                <!-- /.card-comment -->

                <!-- /.card-comment -->
              </div>
        <!-- end Makki -->
