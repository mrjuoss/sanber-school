<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="pb-3 mt-3 mb-3 user-panel d-flex">
        <div class="image">
            @if (is_null(Auth::user()->profile))
            <img src="{{ asset('avatar.jpg') }}" width="100" class="img-circle elevation-2" alt="User Photo">
            @else
            <img src="{{ asset('storage/'.Auth::user()->profile['photo']) }} " width="100"
                class="img-circle elevation-2" alt="User Photo">
            @endif
        </div>
        <div class="info">
            @if(is_null(Auth::user()->profile))
            <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            @else
            <a href="#" class="d-block">{{ Auth::user()->profile['first_name'] }}
                {{ Auth::user()->profile['last_name'] }}</a>
            @endif
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Home
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/home" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Home</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        Profile
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/dashboard/profile/" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Profile</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-newspaper"></i>
                    <p>
                        News
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ url('/dashboard/news')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>News</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-comments"></i>
                    <p>
                        Comments
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ url('/dashboard/comment')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Comments</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file-export"></i>
                    <p>
                        Report
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ url('/dashboard/report/users')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Users</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/dashboard/report/news')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>News</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/dashboard/comments/excel')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Comments</p>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
