<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="ml-auto navbar-nav">
        <li class="mr-1 nav-item">
            {{-- <a class="nav-link">Logout</a> --}}
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button class="nav-item btn btn-primary btn-sm" style="cursor: pointer">
                    <i class="mr-2 fas fa-sign-out-alt"></i>Logout</button>
            </form>
        </li>
    </ul>
</nav>
