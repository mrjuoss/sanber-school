@extends('layouts.backend.master')

@section('content')

<div class="ml-3">
    <h4>{{$comment->nama}}</h4>
    <p>{{$comment->body}}</p>
</div>

@endsection