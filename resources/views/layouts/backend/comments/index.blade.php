@extends('layouts.backend.master')

@section('page_info')
<a href="{{ url('/dashboard/comment/create')}}" class="px-4 mt-2 btn btn-primary btn-sm">
    <i class="mr-2 fas fa-plus"></i>Tambah Data</a>
<a class="px-4 mt-2 btn btn-primary btn-sm" href="/dashboard/comments/pdf">
    <i class="mr-2 fas fa-file-pdf"></i>Download pdf</a>
<a class="px-4 mt-2 btn btn-primary btn-sm" href="/dashboard/comments/excel">
    <i class="mr-2 fas fa-file-excel"></i>Download Excel</a>
@endsection

@section('page_active', 'Comment')
@section('action', 'Index')

@section('content')

<div class="ml-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Comment List</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Commentar</th>
                    <th>Judul Berita</th>
                    <th>Tanggal Komentar</th>
                    <th style="width: 40px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $comment)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $comment->nama }}</td>
                    <td>{{ $comment->body }}</td>
                    <td>{{ $comment->news->title }}</td>
                    <td>{{ $comment->updated_at }}</td>
                    <td style="display:flex;">
                        <a href="/dashboard/comment/{{$comment->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/dashboard/comment/{{$comment->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                        <form action="/dashboard/comment/{{$comment->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        </form>
    </div>

</div>

@endsection
