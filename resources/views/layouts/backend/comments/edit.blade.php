@extends('layouts.backend.master')

@section('content')

    <div class="ml-3">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Comment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/dashboard/comment/{{$comment->id}}" method="POST">
              @csrf
              @method('PUT') 
                  <div class="form-group">
                    <label for="body">Komentar</label>
                    <textarea class="form-control" id="body" name="body" rows="10">{{old('title',$comment->body)}}</textarea>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
        </div>
    </div>
    
@endsection

