@extends('layouts.backend.master')

@section('content')
    <div class="ml-3">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Comment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/dashboard/comment" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">

                    <label for="body">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama anda">

                    <br>               
                    <label for="body">News id</label>
                    <br>

                    <select id="news_id" name="news_id">                
                        @foreach($news as $key => $berita)
                        <option>{{$berita->id}}</option>
                          <!-- <option> {{$berita->id}} - {{$berita->title}}</option> -->
                        @endforeach
                    </select>
                    
                    <br><br>

                    <label for="body">Komentar</label>
                    <textarea class="form-control" id="body" name="body" rows="10"></textarea>
                    
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
        </div>
    </div>
    
@endsection

