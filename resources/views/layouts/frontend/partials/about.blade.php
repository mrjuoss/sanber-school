<section id="about" class="about">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>About Us</h2>
        </div>

        <div class="row content">
            <div class="col-lg-6">
                <p>
                    SanberCode menghadirkan trainer berpengalaman dan bahan pembelajaran pilihan dengan konsep
                    kelas intensif full online selama 4 pekan. Peserta fleksibel menentukan waktu belajar, cukup
                    sediakan sekitar 3-4 jam setiap hari untuk menyelesaikan materi dan tugas.
                </p>
                <ul>
                    <li><i class="ri-check-double-line"></i> Belajar Intensif</li>
                    <li><i class="ri-check-double-line"></i> Teknologi Populer</li>
                    <li><i class="ri-check-double-line"></i> From Zero to Hero</li>
                </ul>
            </div>
            <div class="pt-4 col-lg-6 pt-lg-0">
                <p>
                    Fasilitas grup diskusi dengan ratusan peserta lainnya dan mulai berbarengan menyelesaikan
                    materi dan tugas setiap harinya, membuat training menjadi lebih seru dan menyenangkan.
                </p>
                <a href="#" class="btn-learn-more">Learn More</a>
            </div>
        </div>

    </div>
</section>
