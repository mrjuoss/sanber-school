<!-- ======= Team Section ======= -->
<section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Team</h2>
            <p>Team Sanber School</p>
        </div>

        <div class="row">

            <div class="col-lg-6">
                <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                    <div class="pic"><img src="{{ asset('frontend/img/team/jaki.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="member-info">
                        <h4>Mohamad Arif Mujaki</h4>
                        <span>Chief Executive Officer</span>
                        <p>Developer from Jakarta</p>
                        <div class="social">
                            <a href=""><i class="ri-twitter-fill"></i></a>
                            <a href=""><i class="ri-facebook-fill"></i></a>
                            <a href=""><i class="ri-instagram-fill"></i></a>
                            <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 col-lg-6 mt-lg-0">
                <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
                    <div class="pic"><img src="{{ asset('frontend/img/team/team-2.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="member-info">
                        <h4>Muchammad Makki</h4>
                        <span>Product Manager</span>
                        <p>Developer from Malang</p>
                        <div class="social">
                            <a href=""><i class="ri-twitter-fill"></i></a>
                            <a href=""><i class="ri-facebook-fill"></i></a>
                            <a href=""><i class="ri-instagram-fill"></i></a>
                            <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 col-lg-6">
                <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
                    <div class="pic"><img src="{{ asset('frontend/img/team/tri.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="member-info">
                        <h4>Tri Maretno</h4>
                        <span>CTO</span>
                        <p>Developer from Yogyakarta</p>
                        <div class="social">
                            <a href=""><i class="ri-twitter-fill"></i></a>
                            <a href=""><i class="ri-facebook-fill"></i></a>
                            <a href=""><i class="ri-instagram-fill"></i></a>
                            <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section><!-- End Team Section -->
