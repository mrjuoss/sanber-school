<!-- ======= Header ======= -->
<header id="header" class="fixed-top" style="background-color:#37517E;">
    <div class="container d-flex align-items-center">

        <h1 class="mr-auto logo"><a href="/">SANBER SCHOOL</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="mr-auto logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/#about') }}">About</a></li>
                <li><a href="{{ url('/news') }}">News</a></li>
                <li><a href="{{ url('/#team') }}">Team</a></li>
                <li><a href="{{ url('/login') }}">Admin</a></li>
            </ul>
        </nav><!-- .nav-menu -->

        <a href="#about" class="get-started-btn scrollto">Get Started</a>

    </div>
</header><!-- End Header -->
