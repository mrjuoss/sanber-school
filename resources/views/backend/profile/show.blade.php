@extends('layouts.backend.master')

@section('page_active', 'Profile')
@section('action', 'Show')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                @if(is_null(Auth::user()->profile))
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Belum Ada Profile. Silahkan melengkapi profile anda melalui form berikut
                        </h3>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="/dashboard/profile" method="post">
                            @csrf
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control"
                                    value="{{ old('first_name') }}">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}">
                            </div>
                            <div class="form-group">
                                <label>Nomor HP</label>
                                <input type="number" name="nomor_hp" class="form-control" value="{{ old('nomor_hp') }}">
                            </div>
                            <div class="form-group">
                                <label>Photo Profile</label>
                                <br />
                                <input id="photo" name="photo" type="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Submit" class="px-4 btn btn-primary btn-sm">
                            </div>
                        </form>
                    </div>
                </div>

                @else
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Profile User</h3>
                    </div>
                    <div class="card-body">
                        <table class="table mb-4">
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <th>{{ $profile->user->name }}</th>
                            </tr>
                            <tr>
                                <td>First Name</td>
                                <td>:</td>
                                <th>{{ $profile->first_name }}</th>
                            </tr>
                            <tr>
                                <td>Last Name</td>
                                <td>:</td>
                                <th>{{ $profile->last_name }}</th>
                            </tr>
                            <tr>
                                <td>Nomor HP</td>
                                <td>:</td>
                                <th>{{ $profile->nomor_hp }}</th>
                            </tr>
                            <tr>
                                <td>Photo</td>
                                <td>:</td>
                                <th>
                                    @if($profile->photo)
                                    <img src="{{ asset('storage/'.$profile->photo) }}" width="100" />
                                    @else
                                    No Photo Profile
                                    @endif
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer">
                        <a href="/dashboard/profile/{{ $profile->id }}/edit" class="px-4 btn btn-primary btn-sm">
                            <i class="mr-2 fas fa-edit"></i>Edit
                            Profile</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
</section> @endsection
