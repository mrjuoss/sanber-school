@extends('layouts.backend.master')

@section('page_active', 'Profile')
@section('action', 'Edit')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Profile User</h3>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="/dashboard/profile/{{ $profile->id }}"
                            method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control"
                                    value="{{ $profile->first_name }}">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control"
                                    value="{{ $profile->last_name }}">
                            </div>
                            <div class="form-group">
                                <label>Nomor HP</label>
                                <input type="number" name="nomor_hp" class="form-control"
                                    value="{{ $profile->nomor_hp }}">
                            </div>
                            <div class="form-group">
                                <label>Photo Profile</label>
                                <br />

                                @if($profile->photo)
                                <span class="px-4 py-2 mb-2 badge badge-primary">Current Photo</span>
                                <br />
                                <img src="{{ asset('storage/'.$profile->photo) }}" width="100" />
                                @else
                                <span class="px-4 py-2 mb-2 badge badge-danger">No Photo Profile</span>
                                @endif
                                <input id="photo" name="photo" type="file" class="form-control">
                                <small class="text-muted">Kosongkan jika tidak ingin merubah photo profile</small>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Submit" class="px-4 btn btn-primary btn-sm">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

@endsection
