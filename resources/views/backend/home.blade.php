@extends('layouts.backend.master')

@section('page_active', 'Dashboard')
@section('action', 'home')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Welcome</h3>
                    </div>
                    <div class="card-body">
                        Selamat Datang di Halaman Backend Sanber School. Silahkan memilih menu navigasi yang telah
                        disediakan.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
