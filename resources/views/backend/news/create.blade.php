@extends('layouts.backend.master')

@push('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/summernote/summernote-bs4.css') }}">
 <!-- Select2 -->
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}>
@endpush

@section('page_active', 'News')
@section('action', 'Create')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create News</h3>
                    </div>
                    <form method="POST" action="{{ route('news.store') }}" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">{{ __('Judul') }}</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" placeholder="Masukan Judul" value="{{ old('title') }}" required autocomplete="title" autofocus>
                            @error('title')
                            <p>Eror :<code>{{ $message }}</code></p>
                            @enderror
                        </div>
                        <div class="mb-3">
                        <label for="body">{{ __('Berita') }}</label>
                        <textarea id="textarea" class="form-control textarea" name="body" style="height: 300px">{{ old('title') }}</textarea>
                        @error('body')
                            <p>Eror :<code>{{ $message }}</code></p>
                        @enderror
                        </div>
                        <div class="form-group">
                            <label>Tag</label>
                            <div class="select2-blue">
                                <select class="select2" name="tags[]" multiple="multiple" data-placeholder="Pilih Tag" data-dropdown-css-class="select2-blue" style="width: 100%;">
                                @foreach($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">Input Gambar</label>
                            <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                            </div>
                            @error('image')
                            <p>Eror :<code>{{ $message }}</code></p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="is_publish" value="0">
                            <label class="form-check-label">Sembunyikan</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>

@endsection

@push('scripts')
<!-- bs-custom-file-input -->
<script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
 <!-- Summernote -->
 <script src="{{ asset('backend/plugins/summernote/summernote-bs4.min.js') }}"></script>
 <!-- Select2 -->
<script src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
        height: 300,
    })
  })

  $(document).ready(function () {
  bsCustomFileInput.init();
});

//Initialize Select2 Elements
$('.select2').select2( {
    tags:true
})
</script>
@endpush
