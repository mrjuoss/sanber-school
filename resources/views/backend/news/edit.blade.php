@extends('layouts.backend.master')

@push('styles')
<link rel="stylesheet" href="{{ asset('backend/plugins/summernote/summernote-bs4.css') }}">
 <!-- Select2 -->
<link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}>
@endpush

@section('page_active', 'News')
@section('action', 'Create')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create News</h3>
                    </div>
                    <form method="POST" action="{{ route('news.update',$data->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">{{ __('Judul') }}</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" placeholder="Masukan Judul" value="{{ old('title',$data->title) }}" required autocomplete="title" autofocus>
                            @error('title')
                            <p>Eror :<code>{{ $message }}</code></p>
                            @enderror
                        </div>
                        <div class="mb-3">
                        <label for="body">{{ __('Berita') }}</label>
                        <textarea id="body" name="body" class="textarea" placeholder="Berita">{{ old('body',$data->body) }}</textarea>
                        @error('news')
                            <p>Eror :<code>{{ $message }}</code></p>
                        @enderror
                        </div>
                        <div class="form-group">
                            <label>Tag</label>
                            <div class="select2-purple">
                                <select class="select2" name="tags[]" multiple="multiple" data-placeholder="Select a State" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                @foreach($tags as $tag)
                                <!-- Halo Master, Help Me  -->
                                <!-- Bagimana menampilkan tag yang berelasi di menu edit agar terselected tapi keseluruhan tabel tag juga keluar -->
                                <!-- Looping semua data di table tag -->
                                <!-- jika id table tag == id table tag yang berelasi dengan berita echo selectd -->
                                <!-- CARA LOOPINGNYA GIMANA?? -->
                                <!-- Kalo ada sample codingnya sangat berterimakasih -->
                                <option value="{{ $tag->id }}"
                                    @foreach($data->tag as $value) 
                                    @if($tag->id == $value->id)
                                    selected
                                    @endif
                                    @endforeach
                                >{{ $tag->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">Input Gambar</label>
                            <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image" value="{{$data->image}}">
                                <label class="custom-file-label" for="image">{{$data->image}}</label>
                            </div>
                            </div>
                            @error('news')
                            <p>Eror :<code>{{ $message }}</code></p>
                            @enderror
                        </div>
                        <div class="form-group">
                            
                            @if($data->is_publish == 0)
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="is_publish" value="1">
                            <label class="form-check-label">Tampilkan</label>
                            </div>
                            @else
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="is_publish" value="0">
                            <label class="form-check-label">Sembunyikan</label>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>

@endsection

@push('scripts')
<!-- bs-custom-file-input -->
<script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
 <!-- Summernote -->
 <script src="{{ asset('backend/plugins/summernote/summernote-bs4.min.js') }}"></script>
 <!-- Select2 -->
<script src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
        height: 300,
    })
  })

  $(document).ready(function () {
  bsCustomFileInput.init();
});

//Initialize Select2 Elements
$('.select2').select2({
    tags:true
})
</script>
@endpush
