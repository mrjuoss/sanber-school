@extends('layouts.backend.master')

@section('page_active', 'News')
@section('action', 'View')

@section('content')

<section class="content">
    <div class="container-fluid">
    <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ $data->title }}</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                {!! $data->body !!}
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              @foreach($data->tag as $tagname)
              <a href="{{ route('tagname', $tagname->id ) }}"><span class="badge bg-success">{{ $tagname->name }}</span></a>
              @endforeach
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
    </div>
</section>

@endsection
