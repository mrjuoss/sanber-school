@extends('layouts.backend.master')

@section('page_info')
<a href="{{ url('/dashboard/news/create')}}" class="px-4 mt-2 btn btn-primary btn-sm">
    <i class="mr-2 fas fa-plus"></i>Tambah Data</a>
@endsection

@section('page_active', 'News')
@section('action', 'Index')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">List News</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Isi</th>
                                    <th>Author</th>
                                    <th>Tags</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $news)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $news->title }}</td>
                                    <td>{!! Str::limit($news->body,300,'(...)') !!}</td>
                                    <td>{{ $news->user->name }}</td>
                                    <td>
                                        @foreach($news->tag as $tagname)
                                        <a href="{{ route('tagname', $tagname->id) }}"><span
                                                class="badge bg-primary">{{ $tagname->name }}</span></a>
                                        @endforeach
                                    </td>
                                    <td>{{ $news->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('news.show', $news->id) }}"><button type="button"
                                                class="btn btn-block btn-default" style="margin:3px;">Lihat</button></a>
                                        <a href="{{ route('news.edit', $news->id) }}"><button type="button"
                                                class="btn btn-block btn-warning" style="margin:3px;">Edit</button></a>
                                        <form action="{{ route('news.destroy', $news->id)}}" method="POST"
                                            class="delete-confirm">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger btn-block" style="margin:3px;"
                                                value="Delete">
                                        </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.delete-confirm').on('submit', function(e){
    var form = this;
    event.preventDefault();
    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            return form.submit();
        }
    });
});
</script>
@endpush
