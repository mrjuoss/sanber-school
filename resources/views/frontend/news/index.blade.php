@extends('layouts.frontend.master')

@section('hero')
@include('layouts.frontend.partials.hero')
@endsection

@section('content')

<section id="services" class="services section-bg">
    <div class="container">
        <div class="section-title">
            <h2>NEWS</h2>
            <p>Daftar Berita dan Artikel yang ditulis oleh Para Trainer Sanber School</p>
        </div>
        <div class="row">
            @foreach($data as $news)
            <div class="mb-5 col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box">
                    <h4><a href="">{{ $news->title }}</a></h4>
                    <p>{!!Str::limit($news->body,200,'(...)') !!}</p>
                    <a href="/news/{{ $news->id }}" class="mt-4 btn btn-primary btn-sm">Read more</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</section>

@endsection

@section('footer')
@include('layouts.frontend.partials.footer')
@endsection
