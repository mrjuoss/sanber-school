@extends('layouts.frontend.master')

@section('content')

<section>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
            <h2>{{ $data->title }}</h2>
                <p>{{ $data->body }}</p>
                <p>Author : {{ $data->user->name }}</p>
            <p>Tags : </p>
            <ul>
                @foreach($data->tag as $tagname) 
                    <li>{{ $tagname->name }}</li>
                @endforeach
            </ul>
            </div>
        </div>
    </div>
</section>


@endsection
