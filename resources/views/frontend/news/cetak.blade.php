<style>
    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        text-align: center;

    }
</style>
<center>
    <h2>{{ $data->title }}</h2>
</center>

<p style="text-align:justify">{!! $data->body !!}</p>

Kata Kunci : @foreach($data->tag as $tagname)
<i><span>{{ $tagname->name }},</span></i>
@endforeach

<footer>
    Copyright &copy; <?php echo date("Y");?> <a href="https://gitlab.com/maretknow">Maretknow</a> | <a
        href="https://gitlab.com/mrjuoss">Jaki</a> | <a href="https://gitlab.com/m_makki7">Makki</a>
</footer>
