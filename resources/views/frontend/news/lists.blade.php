@extends('layouts.frontend.master')

@section('content')

<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Judul</th>
							<th>Cover</th>
							<th>Isi</th>
							<th>Tag</th>
							<th>Author</th>
							<th>Tanggal Diedit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $news)
						<tr>
							<td>{{ $news->title }}</td>
							<td>{{ $news->image }}</td>
							<td>{{ $news->body }}</td>
							<td>
								<ul>
									@foreach($news->tag as $tagname)
									<li> {{ $tagname->name }} </li>
									@endforeach
								</ul>
							</td>
							<td>{{ $news->user->name }}</td>
							<td>{{ $news->updated_at }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

@endsection