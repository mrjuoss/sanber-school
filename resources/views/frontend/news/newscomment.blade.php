@extends('layouts.frontend.master')

@section('content')


<section>
<div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                    <br>
                  <!-- <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image"> -->
                                    
                  
                <h1>Title: {{ $data->title }}</h1>
                <p><span class="username"><a href="#">{{ $data->user->name }}</a></span>
                    <span class="description"> {{ $data->updated_at }}</span>
                </p>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                    <i class="far fa-circle"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- post text -->
                <p>{{ $data->body }}</p>

               
                <!-- Social sharing buttons -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button>
                <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                
                <span class="float-right text-muted">45 likes - {{count($comment)}} comments</span>
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments">
                <div class="card-comment">
                  <!-- /.comment-text -->  
                  @foreach($comment as $komentar)               
                  <div class="card-footer card-comments">                    
                        <!-- /.card-comment -->
                        <div class="card-comment">
                        
                            <div class="comment-text">
                                <p>
                                    {{ $komentar->body }}
                                    <br>                            
                                    <span class="text-muted float-right"><a href="#"> {{ $komentar->nama }}</a>, {{$komentar->updated_at}}</span> 
                                    <br>                           
                                </p>
                            </div>
                        <!-- /.comment-text -->
                        </div>
                        <!-- /.card-comment -->
                    </div>
                    <br>
                    @endforeach

                </div>
                <!-- /.card-comment -->

                <!-- /.card-comment -->
              </div>
           
            </div>

            <form role="form" action="/dashboard/comment" method="POST">
              @csrf
                <input type="text" class="form-control" id="news_id" name="news_id" value="{{ $data->id}}" hidden>
                <div class="card-body">
                  <div class="form-group">

                    <label for="body">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama anda">

                    <label for="body">Komentar</label>
                    <textarea class="form-control" id="body" name="body" rows="10"></textarea>
                    
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>

</section>



@endsection
