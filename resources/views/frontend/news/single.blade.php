@extends('layouts.frontend.master')

@section('content')
<section class="mt-4">
    <div class="container mt-4">
        <div class="section-title">
            <h2>NEWS</h2>
            <p>Detail Berita dan Artikel yang ditulis oleh Para Trainer Sanber School</p>
        </div>
        <div class="row">
            <div class="mb-5 col-xl-12 col-md-12 d-flex flex-column align-items-stretch" data-aos="zoom-in"
                data-aos-delay="100">
                <img src="{{ asset('frontend/upload/'.$data->image) }}" alt="News Image" class="img-fluid" />
                <div class="mt-2">
                    <h2>{{ $data->title }}</h2>
                    <p>{!! $data->body !!}</p>
                </div>

                <div class="mt-4">
                    <p>Ditulis oleh : {{ $data->user['name'] }}</p>
                    <p>Ditulis Tanggal : {{ $data->created_at }}</p>
                    <p>Diubah Tanggal : {{ $data->updated_at }}</p>
                </div>
            </div>
        </div>

        <!-- start Makki -->
            <!-- Social sharing buttons -->
             
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments"> 
                <br>
                <?php
                    $likes=mt_rand(101,999);
                ?>
                <span class="float-right text-muted">{{$likes}} likes - {{count($comment)}} comments</span>
                <br>
                <div class="card-comment">
                
                  <!-- /.comment-text -->  
                  @forelse($comment as $komentar)               
                  <div class="card-footer card-comments">                    
                        <!-- /.card-comment -->
                        <div class="card-comment">
                        
                            <div class="comment-text">
                                <p>
                                    {{ $komentar->body }}
                                    <br>                            
                                    <span class="text-muted float-right"><a href="#"> {{ $komentar->nama }}</a>, {{$komentar->updated_at}}</span> 
                                    <br>                           
                                </p>
                            </div>
                        <!-- /.comment-text -->
                        </div>
                        <!-- /.card-comment -->
                    </div>

                    @empty
                        <p align="center"> - No comment - </p>

                    <br>
                    @endforelse

                </div>
                <!-- /.card-comment -->

                <!-- /.card-comment -->
              </div>
        <!-- end Makki -->

        <a href="{{ route('topdf',$data->id)}}" class="btn btn-info btn-sm ml-3 mr-3">Export This News to Pdf</a>

        <h2 class="mt-2 ml-3">Leave a comment</h2>
        <div class="row">
            <div class="mt-4 col-md-10 ml-3">

            <form role="form" action="/news" method="POST">
              @csrf
                <input type="text" class="form-control" id="news_id" name="news_id" value="{{ $data->id}}" hidden>
                <div class="card-body">
                  <div class="form-group">

                    <label for="body">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Name" required>

                    <label for="body">Komentar</label>
                    <textarea class="form-control" id="body" name="body" rows="10" placeholder="Leave a commant" required></textarea>
                    
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>

                <!-- <form action="/news" method="POST" role="form" class="php-email-form" value="{{ $data->id}}">
                    @csrf

                    <div class="form-group">
                        <label for="nama">Your Name</label>
                        <input type="text" name="nama" class="form-control" id="nama" data-rule="minlen:4"
                            data-msg="Please enter at least 4 chars" />
                        <div class="validate"></div>
                    </div>

                    <div class="form-group">
                        <label for="body">Your Comment</label>
                        <textarea class="form-control" name="body" rows="5" data-rule="required"
                            data-msg="Please write something for us"></textarea>
                        <div class="validate"></div>
                    </div>

                    <div class="card-footer">                
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form> -->
            </div>
        </div>

    </div>
</section>
@endsection

@section('footer')
@include('layouts.frontend.partials.footer')
@endsection
