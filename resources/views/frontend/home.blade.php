@extends('layouts.frontend.master')

@section('hero')
@include('layouts.frontend.partials.hero')
@endsection

@section('about')
@include('layouts.frontend.partials.about')
@endsection

{{-- @section('news')
@include('layouts.frontend.partials.news')
@endsection --}}

@section('teams')
@include('layouts.frontend.partials.teams')
@endsection

@section('footer')
@include('layouts.frontend.partials.footer')
@endsection
