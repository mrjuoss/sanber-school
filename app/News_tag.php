<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News_tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news_tag';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','news_id','tag_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
