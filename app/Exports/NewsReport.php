<?php
namespace App\Exports;

use App\News;
use Maatwebsite\Excel\Concerns\FromCollection;

class NewsReport implements FromCollection
{
    public function collection()
    {
        return News::all();
    }
}
