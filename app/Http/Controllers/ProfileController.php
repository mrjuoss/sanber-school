<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Profile::with('user')->get();
        return view('sbadmin2.menu.profile', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:45',
            'last_name' => 'required|string|max:45',
            'nomor_hp' => 'required|string|max:45',
        ]);

        $profile = new Profile;

        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->nomor_hp = $request->get('nomor_hp');

        $photo = $request->file('photo')->store('photo', 'public');
        $profile->photo = $photo;
        $profile->user_id = Auth::id();

        $profile->save();

        Alert::success('Profile', 'Berhasil Membuat Profile User');

        return redirect('/dashboard/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user_id = Auth::id();
        $profile = Profile::where('user_id', $user_id)->first();

        return view('backend.profile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::where('id', $id)->first();

        return view('backend.profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:45',
            'last_name' => 'required|string|max:45',
            'nomor_hp' => 'required|string|max:45',
        ]);

        $profile = Profile::findOrFail($id);

        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->nomor_hp = $request->get('nomor_hp');

        if ($request->file('photo')) {
            if ($profile->photo && file_exists(storage_path('app/public'.$profile->photo))) {
                \Storage::delete('public/'.$profile->photo);
            }

            $photo = $request->file('photo')->store('photo', 'public');
            $profile->photo = $photo;
        }

        $profile->save();

        Alert::success('Profile', 'Berhasil melakukan update profile User');

        return redirect('/dashboard/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
