<?php

namespace App\Http\Controllers;

use Excel;
use App\Exports\UserReport;
use App\Exports\NewsReport;

class ReportController extends Controller
{
    public function reportUserToExcel()
    {
        return Excel::download(new UserReport, 'users.xlsx');
    }

    public function reportNewsToExcel()
    {
        return Excel::download(new NewsReport, 'news.xlsx');
    }
}
