<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagController extends Controller
{
    /* Get All Data With Id Tag */
    public function filter(Tag $tag)
    {
        $data = Tag::find($tag->id);
        return view('backend.news.index', ['data' => $data->news]);
    }
}
