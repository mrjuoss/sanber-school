<?php

namespace App\Http\Controllers;

use App\News;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = News::with(['user','tag'])->get();
        return view('backend.news.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('backend.news.create',['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $arrIdTag=[];
        foreach($request->tags as $value) {
            //cherk apakah valuenya integer
            if(is_numeric($value)){
                // cari tag dalam value
                // kalo gak ada bvaluenya di create
                // kalo gak ada langhsung di push ke array
                if(Tag::find($value) ==  null) {
                    //kita buat database baru numeric
                    Tag::create([
                        'name' => $value,
                    ]);

                    $idTags = Tag::where('name', $value)->first();

                    $arrIdTag[] = strval($idTags['id']);

                } else {
                    //gak ada di databse
                     $arrIdTag[] = strval($value);
                }

            }else {

                if(Tag::where('name', $value)->first() == null){
                    Tag::create([
                        'name' => $value,
                    ]);
    
                    $idTags = Tag::where('name', $value)->first();
                    
                    $arrIdTag[] = strval($idTags['id']);
                } else {
                    $idTags = Tag::where('name', $value)->first();
                    
                    $arrIdTag[] = strval($idTags['id']);
                }
            }
        }

        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('frontend/upload'), $imageName);

        $news = News::create([
            'title' => $request['title'],
            'image' => $imageName,
            'body' => $request['body'],
            'is_publish' => $request['is_publish'],
            'user_id' => Auth::id()
        ]);

        $news->tag()->attach($arrIdTag);

        Alert::success('Success Title', 'Data Telah Tersimoan');

        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //$data = News::find($news)->first();
        $tags = Tag::all();
        return view('backend.news.view',['data'=>$news, 'tags' => $tags]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //echo $news;
        //$data = News::where('id',$news->id)->first();
        $tags = Tag::all();
        return view('backend.news.edit',['data'=>$news, 'tags' => $tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        if($request['image'] == null) {
            $imageName=$news->image;
        } else {
            unlink('frontend/upload/'.$news->image);
            //$imageName = 'sampeimag.jpg';
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('frontend/upload'), $imageName);
        };

        if ($request['is_publish'] == null) {
            $is_publish=$news->is_publish;
        } else {
            $is_publish=$request['is_publish'];
        };

        ///array converter array
         $arrIdTag=[];
        foreach($request->tags as $value) {
            //cherk apakah valuenya integer
            if(is_numeric($value)){
                // cari tag dalam value
                // kalo gak ada bvaluenya di create
                // kalo gak ada langhsung di push ke array
                if(Tag::find($value) ==  null) {
                    //kita buat database baru numeric
                    Tag::create([
                        'name' => $value,
                    ]);

                    $idTags = Tag::where('name', $value)->first();

                    $arrIdTag[] = strval($idTags['id']);

                } else {
                    //gak ada di databse
                     $arrIdTag[] = strval($value);
                }

            }else {

                if(Tag::where('name', $value)->first() == null){
                    Tag::create([
                        'name' => $value,
                    ]);
    
                    $idTags = Tag::where('name', $value)->first();
                    
                    $arrIdTag[] = strval($idTags['id']);
                } else {
                    $idTags = Tag::where('name', $value)->first();
                    
                    $arrIdTag[] = strval($idTags['id']);
                }
            }
        }


        News::where('id', $news->id)->update([
            'title' => $request['title'],
            'image' => $imageName,
            'body' => $request['body'],
            'is_publish' => $is_publish,
            'user_id' => Auth::id()
        ]);


        $news->tag()->sync($arrIdTag);

        Alert::success('Success Title', 'Data Telah Di Update');

        return redirect()->route('news.index');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {   
        if(file_exists('frontend/upload/'.$news->image)){
        unlink('frontend/upload/'.$news->image);
        }
        News::destroy($news->id);
        Alert::success('Success Title', 'Data Telah Di Update');
        return redirect()->route('news.index');
    }
}
