<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function test1() {
        $pdf = PDF::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
    }

    public function test2(){
        $data =view('layouts.backend.comments.index');
        $pdf = PDF::loadView('layouts.pdf.test', compact('data'));
        return $pdf->download('download.pdf');        
    }
}

