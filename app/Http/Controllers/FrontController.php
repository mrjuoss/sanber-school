<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\News;
use App\Comment;

class FrontController extends Controller
{
    /**
     * Display a listing of News.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = News::All();
        // print_r($data);
        return view('frontend.news.lists', ['data' => $data]);
    }


    /**
     * Display a detail news.
     *
     * @return \Illuminate\Http\Response
     */
    public function shownews($id)
    {
        $data = News::find($id);
        $comment= Comment::where('news_id',$id)->get();
        // dd($comment);
        // return view('frontend.news.detail', ['data' => $data]);
        return view('frontend.news.newscomment', ['data' => $data,'comment'=>$comment]);
    }

    public function indexNews()
    {
        $data = News::where('is_publish', 1)->get();

        return view('frontend.news.index', compact('data'));
    }

    public function detailNews($id)
    {
        $data = News::findOrFail($id);
        $comment=Comment::where('news_id',$id)->get();
        // dd($comment);
        return view('frontend.news.single', compact(['data','comment']));
    }

    public function commentNews(Request $request)
    {
        // $news_id = $request->get('id');

        $comment= new Comment;
        $comment->nama=$request["nama"];
        $comment->body=$request["body"];
        $comment->news_id=$request["news_id"];
        $comment->is_banned=0;
        $comment->save();

        return redirect('/news/'.$request["news_id"]);
    }


    public function StoreComment(Request $request)
    {
        $comment= new Comment;
        // $comment->title=$request("title");
        $comment->nama=$request["nama"];
        $comment->body=$request["body"];
        $comment->news_id=$request["news_id"];
        $comment->is_banned=0;
        $comment->save();

        return redirect('/news/'.$request["news_id"]); 
        // return redirect('/comments');   
    }

    /**
     * Converting PDF news.
     *
     * @return \Illuminate\Http\Response
     */
    public function topdf($id)
     {
         $data = News::find($id);
         $pdf = PDF::loadView('frontend.news.cetak', ['data' => $data]);
         return $pdf->stream('cetak.pdf');
     }

    public function PdfFile($id){
        $data = News::find($id);
        $comment=Comment::where('news_id',$id)->get();
        
        $pdf = PDF::loadView('layouts.pdf.NewsPdf', compact(['data','comment']));
        //dd($pdf);
        return $pdf->download('NewsShow.pdf');        
    }

}
