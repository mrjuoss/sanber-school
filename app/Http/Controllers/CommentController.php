<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\News;
use PDF;
use App\Exports\CommentExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Comment::all();
        return view('layouts.backend.comments.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Comment::get();
        $news = News::get();
        return view('layouts.backend.comments.create', ['data' => $data, 'news'=>$news]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment= new Comment;

        $comment->nama=$request["nama"];
        $comment->body=$request["body"];
        $comment->news_id=$request["news_id"];
        $comment->is_banned=0;
        $comment->save();
        
        Alert::success('Success', 'Comment added succesfully');

        // return redirect('/comments'.$request["news_id"]);
        return redirect('/dashboard/comment');   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment=Comment::find($id);

        return view('layouts.backend.comments.show',compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment=Comment::find($id);

        return view('layouts.backend.comments.edit',compact('comment'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query=Comment::where('id',$id)->update([
            // "title"=>$request["title"],
            "body"=>$request["body"],
        ]);
            
        return redirect('/dashboard/comment')->with('success','Berhasil Update Post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::destroy($id);
        
        return redirect('/dashboard/comment')->with('success','Berhasil Dihapus!');
    }

    public function generateInvoice()
    {
        //GET DATA BERDASARKAN ID
        // $invoice = Comment::with(['customer', 'detail', 'detail.product'])->find($id);
        $invoice = Comment::all();

        //LOAD PDF YANG MERUJUK KE VIEW PRINT.BLADE.PHP DENGAN MENGIRIMKAN DATA DARI INVOICE
        //KEMUDIAN MENGGUNAKAN PENGATURAN LANDSCAPE A4
        $pdf = PDF::loadView('layouts.backend.comments.pdftest', compact('invoice'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function CetakPDF(){
        $data = Comment::all();
        $pdf = PDF::loadView('layouts.pdf.test', compact('data'));
        return $pdf->download('comments.pdf');
    }

    public function CetakExcel() 
    {
        return Excel::download(new CommentExport, 'comments.xlsx');
    }

}
