<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','image','body','user_id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_publish' => 'boolean',
    ];

    /**
     * Get the user that create a News.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the comment record associated with the News.
     */
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }
    
    /**
     * Get the tag record associated with the News.
     */
    public function tag()
    {
        return $this->belongsToMany('App\Tag');
    }


}
