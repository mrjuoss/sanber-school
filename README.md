# Final Project with Sanber Code Batch 18

## About This Project

This final project is a school website with the name Sanber School which has the following features:
- Front End
  - See the Main Home of the Website
  - Viewing News List
  - See News Details
  - Add comments on news

- Back End
  - CRUD User
  - CRUD Profile User
  - CRUD Berita (News)
  - Banned Comment News

## Develop by

- [Mohamad Arif Mujaki](https://gitlab.com/mrjuoss)
- [Muchammad Makki](https://gitlab.com/m_makki7)
- [Tri Maretno](https://gitlab.com/maretknow)


## About This Project

This final project is a school website with the name Sanber School which has the following features:
- Front End
  - See the Main Home of the Website
  - Viewing News List
  - See News Details
  - Add comments on news

- Back End
  - Create User
  - Management Profile User
  - Management Berita (News)
  - Report

## Develop by

- [Mohamad Arif Mujaki](https://gitlab.com/mrjuoss)
- [Muchammad Makki](https://gitlab.com/m_makki7)
- [Tri Maretno](https://gitlab.com/maretknow)


## Task TODO
- [x] Menentukan Tema Final Project
- [x] Menentukan Nama Project
- [x] Mendesain Tampilan Kasar UI Final Project
- [x] Mendesain ERD
- [x] Menambahkan File Asset Back End
- [x] Menambahkan File Asset Front End
- [x] Menyiapkan File Migratio
- [x] CRUD Master Table for Back End

## Documentation


## Laravel Library/Package Used
- [Laravel Sweet Alert](https://realrashid.github.io/sweet-alert/)
- [Laravel DOmPDF](https://github.com/barryvdh/laravel-dompdf)
- [Laravel Excel](https://github.com/maatwebsite/Laravel-Excel)
- [Laravel Debuggar](https://github.com/barryvdh/laravel-debugbar)
