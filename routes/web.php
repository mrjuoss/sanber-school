<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});

Route::get('/test', function () {
    return view('layouts.frontend.master');
});

Route::get('/news', 'FrontController@indexNews');
Route::POST('/news', 'FrontController@StoreComment');
Route::get('/news/{id}', 'FrontController@detailNews');
Route::post('/news/{id}/comment', 'FrontController@commentNews');
Route::get('/news/pdf', 'FrontController@PdfFile');


Route::get('/news/detail', function () {
    return view('frontend.news.detail');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/master', function () {
//     return view('sbadmin2.gambar');
// });

//ini hanya contoh boleh dihapus (Makki)
// Route::get('/profile',function(){
// 	return view('sbadmin2.menu.profile');
// });

// Route::get('/news', function () {
//     return view('sbadmin2.menu.news');
// });
// Route::get('/comment', function () {
//     return view('sbadmin2.menu.comment');
// });
// Route::get('/download', function () {
//     return view('sbadmin2.menu.download');
// });


// Route::get('/news', function () {
//     return view('sbadmin2.menu.news');
// });
// Route::get('/comment', function () {
//     return view('sbadmin2.menu.comment');
// });
// Route::get('/download', function () {
//     return view('sbadmin2.menu.download');
// });


Route::get('/berita', 'FrontController@index')->name('berita');
Route::get('/berita/{id}', 'FrontController@shownews')->name('lihatberita');

// Route::resource('dashboard/profile', 'ProfileController');
// Route::resource('dashboard/news', 'NewsController');
// Route::resource('dashboard/comment', 'CommentController');

Route::resource('dashboard/profile', 'ProfileController');
Route::resource('dashboard/news', 'NewsController');
Route::resource('dashboard/comment', 'CommentController');

Route::get('/dashboard/comments/pdf', 'CommentController@CetakPDF');
Route::get('/dashboard/comments/excel', 'CommentController@CetakExcel');
// Route::resource('/dashboard/comments', 'CommentController');


Route::get('/test1', 'PdfController@test1');
Route::get('/test2', 'PdfController@test2');

Route::get('/dashboard/profile', 'ProfileController@show')->middleware('auth');
Route::post('/dashboard/profile', 'ProfileController@store');
Route::get('/dashboard/profile/{profile_id}/edit', 'ProfileController@edit');
Route::put('/dashboard/profile/{profil_id}', 'ProfileController@update');

// Report
Route::get('/dashboard/report/users', 'ReportController@reportUserToExcel');
Route::get('/dashboard/report/news', 'ReportController@reportNewsToExcel');

Route::get('topdf/{id}', 'FrontController@topdf')->name('topdf');
Route::get('tag/{tag}', 'TagController@filter')->name('tagname');
