<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'image' => $faker->unixTime.'.jpg',
        'body' => $faker->text($maxNbChars = 300),
        'is_publish' => $faker->numberBetween($min = 0, $max = 1),
        'user_id' => $faker->numberBetween($min = 1, $max = 3),
    ];
});
