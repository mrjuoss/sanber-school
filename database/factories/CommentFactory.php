<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'body' => $faker->text($maxNbChars = 300),
        'is_banned' => $faker->numberBetween($min = 0, $max = 1),
        'news_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});
