<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News_tag;
use Faker\Generator as Faker;

$factory->define(News_tag::class, function (Faker $faker) {
    return [
        'news_id' => $faker->numberBetween($min = 1, $max = 5),
        'tag_id' => $faker->numberBetween($min = 1, $max = 5)
    ];
});
