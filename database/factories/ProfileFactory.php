<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'nomor_hp' => $faker->e164PhoneNumber,
        'user_id' => $faker->numberBetween($min = 1, $max = 5)
    ];
});
