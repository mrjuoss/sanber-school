<?php

use Illuminate\Database\Seeder;

class News_tagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\News_tag::class, 10)->create();
    }
}
